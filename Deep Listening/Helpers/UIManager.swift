//
//  UIManager.swift
//  Deep Listening
//
//  Created by MacLover on 8/16/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import Foundation
import UIKit

class UIManager {
    
    static func gradientBackground(_ backgroundview: UIView) {
        let toplayer = CAGradientLayer()
        toplayer.frame = CGRect(x: backgroundview.layer.bounds.origin.x, y: backgroundview.layer.bounds.origin.y, width: backgroundview.layer.bounds.size.width, height: backgroundview.layer.bounds.size.height*2/3)
        toplayer.colors = [UIColor(red: 17/255, green: 19/255, blue: 23/255, alpha: 1.0).cgColor, UIColor(red: 18/255, green: 66/255, blue: 89/255, alpha: 1.0).cgColor]
        backgroundview.layer.addSublayer(toplayer)
        
        let bottomlayer = CAGradientLayer()
        bottomlayer.frame = CGRect(x: backgroundview.layer.bounds.origin.x, y: backgroundview.layer.bounds.size.height*2/3, width: backgroundview.layer.bounds.size.width, height: backgroundview.layer.bounds.size.height/3)
        bottomlayer.colors = [UIColor(red: 18/255, green: 66/255, blue: 89/255, alpha: 1.0).cgColor, UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0).cgColor]
        backgroundview.layer.addSublayer(bottomlayer)
        
    }
    
}
