//
//  QuestionCell.swift
//  Deep Listening
//
//  Created by MacLover on 8/4/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit


class QuestionCell: UITableViewCell {
    @IBOutlet var questionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        if selected {
//            questionLabel.textColor = UIColor(red: 72/255, green: 72/255, blue: 72/255, alpha: 1.0)
//            self.selectionStyle = .none
//        } else {
//            questionLabel.textColor = UIColor.white
//        }
        
        
    }
    
}

extension QuestionCell {
    var tableView: UITableView? {
        return superview as? UITableView
    }
    var indexPath: IndexPath? {
        return tableView?.indexPath(for: self)
    }
}
