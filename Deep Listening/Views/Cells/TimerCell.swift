//
//  TimerCell.swift
//  Deep Listening
//
//  Created by MacLover on 8/8/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit
var timer: Timer?
class TimerCell: UITableViewCell {

    @IBOutlet var lblTimer: UILabel!
    private var timeCounter: Double = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var expiryTimeInterval : TimeInterval? {
         didSet {
            if timerIsOff! {
                endTimer()
            } else {
                lblTimer.text = self.displayTimeLeft()
                startTimer()
            }
            
         }
     }
    
    private func endTimer() {
        timer?.invalidate()
        timer?.fireDate = Date.distantFuture
        timer = nil
        return
    }

    private func startTimer() {
        if let interval = expiryTimeInterval {
            timeCounter = interval
            timer = Timer(timeInterval: 1, target: self, selector:#selector(countdown), userInfo: nil, repeats: true)
            RunLoop.current.add(timer!, forMode: .common)
        }
     }

    @objc func countdown() {
                
        guard timeCounter >= 0 else {
            timer?.invalidate()
            timer?.fireDate = Date.distantFuture
            timer = nil
            return
         }
        
        guard (timer != nil) else {
            return
        }
        ListeningTime.sec! -= 1
         
        if (ListeningTime.sec! < 0)
         {
             ListeningTime.sec = 59
            ListeningTime.min! -= 1
             
             if (ListeningTime.min! < 0)
             {
                 ListeningTime.min = 59
                 ListeningTime.hour! -= 1
                 
                 if (ListeningTime.hour! < 0)
                 {
                     ListeningTime.hour = 0
                 }
             }
         }
        
        if ListeningTime.sec == 0 && ListeningTime.min == 0 && ListeningTime.hour == 0 {
            timer?.invalidate()
            timer?.fireDate = Date.distantFuture
            timer = nil
        }
        
        lblTimer.text = displayTimeLeft()
        timeCounter -= 1
     }
    
    func displayTimeLeft() -> String {
        
        var seconds = "\(ListeningTime.sec!)"
        var minutes = "\(ListeningTime.min!)"
        var hours = "\(ListeningTime.hour!)"
        
        if (ListeningTime.sec! < 10)
        {
            seconds = "0\(ListeningTime.sec!)"
        }
        if (ListeningTime.min! < 10)
        {
            minutes = "0\(ListeningTime.min!)"
        }
        if (ListeningTime.hour! < 10)
        {
            hours = "0\(ListeningTime.hour!)"
        }
        
        let timeleft = hours + ":" + minutes + ":" + seconds
        
        return timeleft
    }

}
