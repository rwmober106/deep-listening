//
//  QuestionLastCell.swift
//  Deep Listening
//
//  Created by MacLover on 8/6/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class QuestionLastCell: UITableViewCell {
    @IBOutlet var btnSetTimer: UIButton!    
    @IBOutlet var btnAskYourOwn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupButtons() {
        let btnSetTimerTitle = NSAttributedString(string: "set your timer", attributes: [
            NSAttributedString.Key.font : UIFont(name: "JosefinSans-Regular", size: 30.0) ?? UIFont.systemFont(ofSize: 15.0),
            NSAttributedString.Key.foregroundColor : UIColor.white])
        btnSetTimer.setAttributedTitle(btnSetTimerTitle, for: .normal)
        
        btnSetTimer.layer.borderWidth = 2
        btnSetTimer.layer.borderColor = UIColor.white.cgColor
        
    }

}
