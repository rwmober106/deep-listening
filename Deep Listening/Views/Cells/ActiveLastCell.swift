//
//  ActiveLastCell.swift
//  Deep Listening
//
//  Created by MacLover on 8/8/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class ActiveLastCell: UITableViewCell {

    @IBOutlet var btnPause: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnPause.clipsToBounds = true
        btnPause.layer.cornerRadius = btnPause.layer.bounds.size.width/2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
