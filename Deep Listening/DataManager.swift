//
//  DataManager.swift
//  Deep Listening
//
//  Created by MacLover on 10/18/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    
    var selectedQuestions: [String]? = nil
    
    //Utilize Singleton pattern by instanciating DataManager only once.
    class var sharedInstance: DataManager {
        struct Singleton {
            static let instance = DataManager()
        }
        return Singleton.instance
    }

    override init() {
        super.init()
        
        selectedQuestions = [String]()
        
    }
}
