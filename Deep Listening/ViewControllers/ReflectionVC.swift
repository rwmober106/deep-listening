//
//  ReflectionVC.swift
//  Deep Listening
//
//  Created by MacLover on 8/4/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class ReflectionVC: UIViewController {
    
    @IBOutlet var btnBegin: UIButton!
    @IBOutlet var btnQuestions: UIButton!
    @IBOutlet var btnTips: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }
    
    func initUI() {
        
        let btnBeginTitle = NSAttributedString(string: "beginning", attributes: [
            NSAttributedString.Key.font : UIFont(name: "JosefinSans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15.0),
            NSAttributedString.Key.foregroundColor : UIColor.white])
        btnBegin.setAttributedTitle(btnBeginTitle, for: .normal)
        
        btnBegin.layer.borderWidth = 2
        btnBegin.layer.borderColor = UIColor.white.cgColor
        
        let btnQuestionsTitle = NSAttributedString(string: "questions", attributes: [
            NSAttributedString.Key.font : UIFont(name: "JosefinSans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15.0),
            NSAttributedString.Key.foregroundColor : UIColor.white])
        btnQuestions.setAttributedTitle(btnQuestionsTitle, for: .normal)
        
        btnQuestions.layer.borderWidth = 2
        btnQuestions.layer.borderColor = UIColor.white.cgColor
        
        let btnTipsTitle = NSAttributedString(string: "tips", attributes: [
            NSAttributedString.Key.font : UIFont(name: "JosefinSans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15.0),
            NSAttributedString.Key.foregroundColor : UIColor.white])
        btnTips.setAttributedTitle(btnTipsTitle, for: .normal)
        
        btnTips.layer.borderWidth = 2
        btnTips.layer.borderColor = UIColor.white.cgColor       
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func onClickedBtnBegin(_ sender: Any) {
        
        for vc in self.navigationController!.viewControllers {
            if vc is IntroVC {
                navigationController?.popToViewController(vc, animated: false)
            }
                
        }
        
    }
    
    @IBAction func onClickedBtnQuestions(_ sender: Any) {
        
        for vc in self.navigationController!.viewControllers {
            if vc is QuestionsVC {
                navigationController?.popToViewController(vc, animated: false)
            }
                
        }
        
    }
    
    @IBAction func onClickedBtnTips(_ sender: Any) {
        
        guard let tipsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TipsVC") as? TipsVC else { return  }
        tipsVC.modalPresentationStyle = .fullScreen
        self.present(tipsVC, animated: true, completion: nil)
        
    }
    
}
