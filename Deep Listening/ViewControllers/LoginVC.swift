//
//  ViewController.swift
//  Deep Listening
//
//  Created by MacLover on 8/2/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet var tfUsername: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
                target: self,
                action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // call the 'keyboardWillShow' function when the view controller receive the notification that a keyboard is going to be shown
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        // call the 'keyboardWillHide' function when the view controlelr receive notification that keyboard is going to be hidden
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    // Unregisters keyboard event observation
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self,
            name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self,
            name: UIResponder.keyboardWillHideNotification, object: nil)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
             // if keyboard size is not available for some reason, dont do anything
             return
          }
        
        // move the root view up by the distance of keyboard height
        self.view.frame.origin.y = 0 - keyboardSize.height
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        // move back the root view origin to zero
        self.view.frame.origin.y = 0
    }

    func initUI() {
        
        //username textfield
        let strUsername = NSAttributedString(string: "choose your username", attributes: [
            NSAttributedString.Key.font : UIFont(name: "JosefinSans-Light", size: 30.0) ?? UIFont.systemFont(ofSize: 30.0),
        NSAttributedString.Key.foregroundColor : UIColor.white])
        tfUsername.attributedPlaceholder = strUsername
        
        //password textfield
        let strPassword = NSAttributedString(string: "password", attributes: [
            NSAttributedString.Key.font : UIFont(name: "JosefinSans-Light", size: 30.0) ?? UIFont.systemFont(ofSize: 30.0),
        NSAttributedString.Key.foregroundColor : UIColor.white])
        tfPassword.attributedPlaceholder = strPassword       
        
        
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        let username = self.tfUsername.text
        guard username != nil && username != ""  else {
            let alertController = UIAlertController(title: "", message: "You must enter user name.", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(actionOk)
            present(alertController, animated: true, completion: nil)
            return
        }
        
        let defaults = UserDefaults.standard
        defaults.set(username, forKey: "username")
        
        let introVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
        navigationController?.pushViewController(introVC, animated: true)
        
    }
    
    
    
}



