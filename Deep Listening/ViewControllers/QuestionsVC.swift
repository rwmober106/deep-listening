//
//  QuestionsVC.swift
//  Deep Listening
//
//  Created by MacLover on 8/4/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit
import FirebaseDatabase
import JGProgressHUD

var timerIsOff: Bool?

struct ListeningTime {
    static var hour: Int?
    static var min: Int?
    static var sec: Int?
    static var totalSeconds: Int?
    
    init(hour: Int? = nil,
         min: Int? = nil,
         sec: Int? = nil,
         totalSeconds: Int? = nil) {
        
        ListeningTime.hour = hour
        ListeningTime.min = min
        ListeningTime.sec = sec
        ListeningTime.totalSeconds = totalSeconds
        
    }
    
}

var questionList: [String] = []

class QuestionsVC: UIViewController {
    
    @IBOutlet var tvQuestions: UITableView!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var ownQuestionTextView: UITextView!
    @IBOutlet var askYourOwnView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    
    private var HUD: JGProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //enable self-sizing table view cells
        self.tvQuestions.rowHeight = UITableView.automaticDimension
        self.tvQuestions.estimatedRowHeight = 500
        
        self.tvQuestions.delegate = self
        self.tvQuestions.dataSource = self
        _ = ListeningTime.init(hour: 0, min: 45, sec: 0, totalSeconds: 2700)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
                target: self,
                action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

        ownQuestionTextView.layer.borderColor = UIColor.white.cgColor
        ownQuestionTextView.layer.borderWidth = 2

        NotificationCenter.default.addObserver(self, selector: #selector(QuestionsVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(QuestionsVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        
        timerIsOff = false
        loadQuestionsFromFirebase()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self,
            name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self,
            name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func initUI() {
        
        //initialize buttons
        btnSave.layer.borderColor = UIColor.white.cgColor
        btnSave.layer.borderWidth = 2
        btnCancel.layer.borderWidth = 2
        btnCancel.layer.borderColor = UIColor.white.cgColor
        
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
             // if keyboard size is not available for some reason, dont do anything
             return
          }
        
        // move the root view up by the distance of keyboard height
        self.view.frame.origin.y = 0 - keyboardSize.height
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        // move back the root view origin to zero
        self.view.frame.origin.y = 0
    }
    
    func loadQuestionsFromFirebase() {
        showHUD(view: self.view)
        let ref = Database.database().reference()
        ref.child("Questions").observe(.value) { (snapshot) in
            self.hideHUD()
            questionList.removeAll()
            for child in snapshot.children {
                if let question = child as? DataSnapshot {
                    let value = question.value as? NSDictionary
                    let keys = value?.allKeys as? [String] ?? []
                    let strQuestion = value?[keys[0]] as? String ?? ""
                    questionList.append(strQuestion)
                }
            }
            
            var myownquestionlist: [String] = []
            self.showHUD(view: self.view)
            let username = UserDefaults.standard.string(forKey: "username")
            let childNode = "\(username!)'s questions"
            ref.child("Custom-Questions").child(childNode).observe(.value) { (snapshot) in
                self.hideHUD()
                for child in snapshot.children {
                    if let question = child as? DataSnapshot {
                        let value = question.value as? NSDictionary
                        let keys = value?.allKeys as? [String] ?? []
                        let strQuestion = value?[keys[0]] as? String ?? ""
                        myownquestionlist.append(strQuestion)
                    }
                }
                questionList += myownquestionlist
                DispatchQueue.main.async {
                    self.tvQuestions.reloadData()
                }
                
            }
            
            
        }
    }
    
    func showHUD(view: UIView) {
        
        self.HUD = JGProgressHUD(style: .extraLight)
        self.HUD?.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        self.HUD?.show(in: view)
        
    }
    
    func hideHUD() {
        if ( self.HUD != nil ){
            if (self.HUD?.isVisible)! {
                self.HUD?.dismiss()
            }
        }
        
    }
    
    @IBAction func onClickedBtnSave(_ sender: Any) {
        
        guard !ownQuestionTextView.text.isEmpty else {
            let alertController = UIAlertController(title: "", message: "Please add your own question.", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(actionOk)
            present(alertController, animated: true, completion: nil)
            return
        }
        var cusQuestion = ""
        let customQuestion = ownQuestionTextView.text as String
        let charset = CharacterSet(charactersIn: "\n")
        if customQuestion.rangeOfCharacter(from: charset) !=  nil {
            cusQuestion = customQuestion.replacingOccurrences(of: "\n", with: " ")
            print("")
        } else {
            cusQuestion = customQuestion
        }
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss Z"
        let datetime = formatter.string(from: now)
        let username = UserDefaults.standard.string(forKey: "username")!
        let childNode = "\(username)'s questions"
        showHUD(view: self.view)
        let ref = Database.database().reference()
        ref.child("Custom-Questions").child(childNode).child(datetime).setValue(["content": cusQuestion]) { (err, dbRef) in
            self.hideHUD()
            if (err) != nil {
                
            } else {
                print("Custom question add success")
                self.ownQuestionTextView.text = ""
                self.loadQuestionsFromFirebase()
            }
        }

    }
    
    @IBAction func onClickedBtnCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.askYourOwnView.frame = CGRect(x: (self.view.bounds.width - self.askYourOwnView.bounds.width)/2, y: self.view.bounds.height, width: self.askYourOwnView.bounds.width, height: self.askYourOwnView.bounds.height)
        }
    }
    
}

extension QuestionsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let numberOfRows = questionList.count + 2
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowIndex = indexPath.row
        if rowIndex == 0 {
            return 417
        } else if rowIndex > 0 && rowIndex < questionList.count + 1 {
            return UITableView.automaticDimension
        } else {
            return 212
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowIndex = indexPath.row
        if rowIndex == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionFirstCell", for: indexPath) as! QuestionFirstCell
            return cell
        } else if rowIndex > 0 && rowIndex < questionList.count + 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionCell
            cell.questionLabel.text = questionList[rowIndex - 1]
            
            let selectionView = UIView()
            selectionView.backgroundColor = .clear
            cell.multipleSelectionBackgroundView = selectionView
            cell.questionLabel.highlightedTextColor = UIColor(red: 72/255, green: 72/255, blue: 72/255, alpha: 1.0)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionLastCell", for: indexPath) as! QuestionLastCell
            cell.setupButtons()
            
            let tapSetTimer = UITapGestureRecognizer(target: self, action: #selector(tapSetTimer(recognizer:)))
            cell.btnSetTimer.addGestureRecognizer(tapSetTimer)
            tapSetTimer.delegate = self as? UIGestureRecognizerDelegate
            
            
            let tapAskYourOwn = UITapGestureRecognizer(target: self, action: #selector(tapAskYourOwn(recognizer:)))
            cell.btnAskYourOwn.addGestureRecognizer(tapAskYourOwn)
            tapAskYourOwn.delegate = self as? UIGestureRecognizerDelegate

            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let rowIndex = indexPath.row - 1
        let question = questionList[rowIndex]
        DataManager.sharedInstance.selectedQuestions?.append(question)
        print("Selected questions: ", DataManager.sharedInstance.selectedQuestions!)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let rowIndex = indexPath.row - 1
        let question = questionList[rowIndex]
        if DataManager.sharedInstance.selectedQuestions?.count != 0 {
            var selectedQuestions = DataManager.sharedInstance.selectedQuestions as! [String]
            for one in selectedQuestions {
                var i = 0
                if question.elementsEqual(one) {
                    print("Deselected the question and removed it from selected list")
                    DataManager.sharedInstance.selectedQuestions?.remove(at: i)
                    print("Selected questions: ", DataManager.sharedInstance.selectedQuestions!)
                }
                i += 1
            }
        }
        
        
    }
    
    @objc func tapSetTimer(recognizer: UITapGestureRecognizer) {
        guard let setTimerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetTimerVC") as? SetTimerVC else { return  }
        navigationController?.pushViewController(setTimerVC, animated: true)
    }
    
    @objc func tapAskYourOwn(recognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5) {
            self.askYourOwnView.frame = CGRect(x: (self.view.bounds.width - self.askYourOwnView.bounds.width)/2, y: (self.view.bounds.height - self.askYourOwnView.bounds.height)/2+100, width: self.askYourOwnView.bounds.width, height: self.askYourOwnView.bounds.height)
        }
    }
    
}


