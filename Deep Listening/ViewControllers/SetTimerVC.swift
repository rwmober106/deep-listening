//
//  SetTimerVC.swift
//  Deep Listening
//
//  Created by MacLover on 8/7/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class SetTimerVC: UIViewController {
    
    @IBOutlet var btnSet: UIButton!
    @IBOutlet var pvTimer: UIPickerView!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    
    private var hours = [Int]()
    private var mins = [Int]()
    private var strTime: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadHoursAndMinutes()
        initUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }

    func initUI() {
        
        btnSet.layer.borderWidth = 2
        btnSet.layer.borderColor = UIColor.white.cgColor
        
        pvTimer.selectRow(44, inComponent: 1, animated: true)
        
    }
    
    @IBAction func onClickedBtnSet(_ sender: Any) {
        
        let hrs = pvTimer.selectedRow(inComponent: 0)
        let mins = pvTimer.selectedRow(inComponent: 1) + 1
        let tSeconds = hrs*3600 + mins*60
        
        _ = ListeningTime.init(hour: hrs, min: mins, sec: 0, totalSeconds: tSeconds)
        
        guard let tipsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TipsVC") as? TipsVC else { return  }
               navigationController?.pushViewController(tipsVC, animated: true)
        
    }
    
    func loadHoursAndMinutes() {
        
        for hour in 0...9 {
            hours.append(hour)
        }
        for min in 1...60 {
            mins.append(min)
        }
        
    }

    
}

extension SetTimerVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (component == 0) {
            return hours.count
        } else {
            return mins.count
        }
    }

    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        var strTitle = ""

        if (component == 0) {
            strTitle = "\(hours[row]) hour"
        } else {
            strTitle = "\(mins[row]) min"
        }

        let rowSize = pickerView.rowSize(forComponent: component)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: rowSize.width, height: rowSize.width))
        label.textColor = UIColor.white
        label.text = strTitle
        label.textAlignment = .center

        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let hour = hours[pickerView.selectedRow(inComponent: 0)]
        let min = mins[pickerView.selectedRow(inComponent: 1)]
        
        var strTimer = ""
        if hour == 0 {
            strTimer = "\(min) min"
        } else {
            strTimer = "\(hour) hr \(min) min"
        }
        
        lblTime.text = strTimer
        
    }
    
    
    
}

