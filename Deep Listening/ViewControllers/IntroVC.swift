//
//  IntroVC.swift
//  Deep Listening
//
//  Created by MacLover on 8/4/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class IntroVC: UIViewController {
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    @IBOutlet var btnBegin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBegin.layer.borderColor = UIColor.white.cgColor
        btnBegin.layer.borderWidth = 2

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
