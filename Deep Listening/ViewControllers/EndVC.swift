//
//  EndVC.swift
//  Deep Listening
//
//  Created by MacLover on 8/4/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class EndVC: UIViewController {
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    @IBAction func onClickedBtnNo(_ sender: Any) {
        
        for vc in navigationController!.viewControllers {
            if vc is IntroVC {
                navigationController?.popToViewController(vc, animated: true)
            }
        }
        
    }
    
    @IBAction func onClickedBtnYes(_ sender: Any) {
        
        guard let reflectionVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReflectionVC") as? ReflectionVC else { return }
        navigationController?.pushViewController(reflectionVC, animated: true)
        
    }
    

}
