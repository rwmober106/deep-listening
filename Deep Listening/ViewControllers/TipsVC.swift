//
//  TipsVC.swift
//  Deep Listening
//
//  Created by MacLover on 8/4/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class TipsVC: UIViewController {
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onClickedBtnGotit(_ sender: Any) {
        let vc = self.presentingViewController?.children.last
        if vc is ReflectionVC {
            dismiss(animated: true, completion: nil)
        } else {
            guard let activeUseVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ActiveUseVC") as? ActiveUseVC else { return  }
            navigationController?.pushViewController(activeUseVC, animated: true)
        }
        
    }
    
}
