//
//  ActiveUseVC.swift
//  Deep Listening
//
//  Created by MacLover on 8/8/20.
//  Copyright © 2020 MacLover. All rights reserved.
//

import UIKit

class ActiveUseVC: UIViewController {

    @IBOutlet var tvActiveUse: UITableView!
    @IBOutlet var buttonContainerView: UIView!
    @IBOutlet var btnSwitch: UIButton!
    @IBOutlet var btnContinue: UIButton!
    @IBOutlet var btnEnd: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundBottomConstraint: NSLayoutConstraint!
    @IBOutlet var backgroundTopConstraint: NSLayoutConstraint!
    @IBOutlet var lblTimer: UILabel!
    @IBOutlet var btnPause: UIButton!
    
    private var timeCounter: Double = 0
    
//    var questionList: [String] = []
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tvActiveUse.delegate = self
        self.tvActiveUse.dataSource = self
        self.tvActiveUse.rowHeight = UITableView.automaticDimension
        self.tvActiveUse.estimatedRowHeight = 500
        
        initUI()

        lblTimer.text = self.displayTimeLeft()
        startTimer()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tvActiveUse.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
        DataManager.sharedInstance.selectedQuestions?.removeAll()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIManager.gradientBackground(backgroundView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func initUI() {
        btnSwitch.layer.borderWidth = 2
        btnSwitch.layer.borderColor = UIColor.white.cgColor
        btnContinue.layer.borderWidth = 2
        btnContinue.layer.borderColor = UIColor.white.cgColor
        btnEnd.layer.borderWidth = 2
        btnEnd.layer.borderColor = UIColor.white.cgColor
        btnPause.clipsToBounds = true
        btnPause.layer.cornerRadius = btnPause.layer.bounds.size.width/2
    }
    
    func endBackgroundTask() {
      print("Background task ended.")
      UIApplication.shared.endBackgroundTask(backgroundTask)
      backgroundTask = .invalid
    }
    
    @IBAction func onClickedBtnSwitch(_ sender: Any) {
        _ = ListeningTime.init(hour: 0, min: 0, sec: 0, totalSeconds: 0)
        timer?.invalidate()
        timer = nil
        UIView.animate(withDuration: 0.5, animations: {
            self.buttonContainerView.frame = CGRect(x: (self.view.bounds.width - self.buttonContainerView.bounds.width)/2, y: self.view.bounds.height, width: self.buttonContainerView.bounds.width, height: self.buttonContainerView.bounds.height)
        }) { (void) in
            for vc in self.navigationController!.viewControllers {
                if vc is QuestionsVC {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
                    
            }
        }
        
    }
    @IBAction func onClickedBtnContinue(_ sender: Any) {
//        timerIsOff = false
        startTimer()
        UIView.animate(withDuration: 0.5) {
            self.buttonContainerView.frame = CGRect(x: (self.view.bounds.width - self.buttonContainerView.bounds.width)/2, y: self.view.bounds.height, width: self.buttonContainerView.bounds.width, height: self.buttonContainerView.bounds.height)
        }
        
    }
    @IBAction func onClickedBtnEnd(_ sender: Any) {
        timer?.invalidate()
        timer = nil
        UIView.animate(withDuration: 0.5, animations: {
            self.buttonContainerView.frame = CGRect(x: (self.view.bounds.width - self.buttonContainerView.bounds.width)/2, y: self.view.bounds.height, width: self.buttonContainerView.bounds.width, height: self.buttonContainerView.bounds.height)
        }) { (void) in
            guard let endVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EndVC") as? EndVC else { return  }
            self.navigationController?.pushViewController(endVC, animated: true)
        }
    }
    
    @IBAction func onClickedBtnPause(_ sender: Any) {
        
//        timerIsOff = true
        endTimer()
        
        UIView.animate(withDuration: 0.6, animations: {
            self.buttonContainerView.frame = CGRect(x: (self.view.bounds.width - self.buttonContainerView.bounds.width)/2, y: (self.view.bounds.height - self.buttonContainerView.bounds.height)/2+100, width: self.buttonContainerView.bounds.width, height: self.buttonContainerView.bounds.height)
        }) { (void) in
//            let topRow = IndexPath(row: 0, section: 0)
//            self.tvActiveUse.scrollToRow(at: topRow,
//                                       at: .top,
//                                       animated: true)
        }
        
    }
    
    
    private func endTimer() {
        timer?.invalidate()
        timer?.fireDate = Date.distantFuture
        timer = nil
        return
    }

    private func startTimer() {
        let totalSeconds = ListeningTime.totalSeconds!
        let interval = TimeInterval(totalSeconds)
        timeCounter = interval
        timer = Timer(timeInterval: 1, target: self, selector:#selector(countdown), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .common)
     }

    @objc func countdown() {
                
        guard timeCounter >= 0 else {
            timer?.invalidate()
            timer?.fireDate = Date.distantFuture
            timer = nil
            return
         }
        
        guard (timer != nil) else {
            return
        }
        ListeningTime.sec! -= 1
         
        if (ListeningTime.sec! < 0)
         {
             ListeningTime.sec = 59
            ListeningTime.min! -= 1
             
             if (ListeningTime.min! < 0)
             {
                 ListeningTime.min = 59
                 ListeningTime.hour! -= 1
                 
                 if (ListeningTime.hour! < 0)
                 {
                     ListeningTime.hour = 0
                 }
             }
         }
        
        if ListeningTime.sec == 0 && ListeningTime.min == 0 && ListeningTime.hour == 0 {
            timer?.invalidate()
            timer?.fireDate = Date.distantFuture
            timer = nil
        }
        
        lblTimer.text = displayTimeLeft()
        timeCounter -= 1
     }
    
    func displayTimeLeft() -> String {
        
        var seconds = "\(ListeningTime.sec!)"
        var minutes = "\(ListeningTime.min!)"
        var hours = "\(ListeningTime.hour!)"
        
        if (ListeningTime.sec! < 10)
        {
            seconds = "0\(ListeningTime.sec!)"
        }
        if (ListeningTime.min! < 10)
        {
            minutes = "0\(ListeningTime.min!)"
        }
        if (ListeningTime.hour! < 10)
        {
            hours = "0\(ListeningTime.hour!)"
        }
        
        let timeleft = hours + ":" + minutes + ":" + seconds
        
        if hours == "00" && minutes == "00" && seconds == "00" {
            UIView.animate(withDuration: 0.5) {
                 self.buttonContainerView.frame = CGRect(x: (self.view.bounds.width - self.buttonContainerView.bounds.width)/2, y: (self.view.bounds.height - self.buttonContainerView.bounds.height)/2+100, width: self.buttonContainerView.bounds.width, height: self.buttonContainerView.bounds.height)
            }
            
            _ = ListeningTime.init(hour: 0, min: 45, sec: 0, totalSeconds: 2700)
            
        }
        
        return timeleft
    }

}

extension ActiveUseVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = questionList.count
        return numberOfRows
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowIndex = indexPath.row
        let question = questionList[rowIndex]
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionCell
        cell.questionLabel.text = question
        cell.questionLabel.textColor = .white
        let selectedQuestions = DataManager.sharedInstance.selectedQuestions!
        if selectedQuestions.count != 0 {
            for one in selectedQuestions {
                if one.elementsEqual(question) {
                    cell.questionLabel.textColor = UIColor(red: 72/255, green: 72/255, blue: 72/255, alpha: 1.0)
                }
            }
        }
        
        return cell
    }

}
